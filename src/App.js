import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import React, { useState } from "react";
import Home from "./components/Home";
import WhoWeAre from "./components/WhoWeAre/WhoWeAre";
import WhatWeDo from "./components/WhatWeDo";
import Join from "./components/Join";
import News from "./components/News";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Policity from "./components/Policity";
import teamJson from "./components/WhoWeAre/team.json";
import PaymentForm from "./components/PaymentForm";


function App() {
  const [team] = useState(teamJson);
  return (
    <Router>
      <div className="App">
        <Navbar />

        <Switch>
          <Route exact path="/" component={Home} />

          <Route
            exact
            path="/quienes-somos"
            render={() => <WhoWeAre team={team} />}
          />
          <Route exact path="/que-hacemos" component={WhatWeDo} />
          <Route exact path="/unete" component={Join} />
          <Route exact path="/actualidad" component={News} />
          <Route exact path="/policity" component={Policity} />
          <Route exact path="/donate" component={PaymentForm} />
        </Switch>

        <Footer />
      </div>
    </Router>
  );
}

export default App;
