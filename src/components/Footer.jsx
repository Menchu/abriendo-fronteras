import React from "react";
import "../styles/Footer.scss";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import {
  faInstagram,
  faYoutube,
  faFacebook,
} from "@fortawesome/free-brands-svg-icons";

export default function Footer() {
  return (
    <div className="Footer">
      <div className="Footer__containerLeft">
        <div className="Footer__containerLeft__rrss">
          <p>Síguenos</p>
          <a href="https://www.facebook.com/Abriendo-Fronteras-Ong-101008695180828">
            <FontAwesomeIcon className="rrss-link" icon={faFacebook} />
          </a>

          <a href="https://www.instagram.com/abriendofronterasong/">
            <FontAwesomeIcon className="rrss-link" icon={faInstagram} />
          </a>

          <a href="https://www.youtube.com/channel/UCVcASvnfbjIzrMcFeB1t_PQ">
            <FontAwesomeIcon className="rrss-link" icon={faYoutube} />
          </a>
        </div>
        <div className="Footer__containerLeft__policity">
          <div className="Footer__containerLeft__policity-contact">
            <a href="mailto:info@abriendofronteras.es">Contacta</a>
            <FontAwesomeIcon className="rrss-link" icon={faEnvelope} />
          </div>

          <Link to="/policity"> Política de Protección de Datos. </Link>
          <p>Todos los derechos reservados para Abriendo Fronteras 2021 ® .</p>
          <p>Calle del Casino, 12. Madrid (Local Ibn Batutta). </p>
          <p>Teléfono: 658 45 03 29</p>
        </div>
      </div>
      <div className="Footer__containerRight">
        <p>Entidades Colaboradoras:</p>
        <div className="Footer__containerRight__img">
        <img src="/assets/solides.png" alt="logo"></img>
        <img src="/assets/ate.png" alt="logo"></img>
        </div>
        

      </div>
    </div>
  );
}
