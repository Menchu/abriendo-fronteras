import React from "react";
import { Link } from "react-router-dom";
import "../styles/Navbar.scss";

export default function Navbar() {
  return (
    <div className="Navbar">
      <nav role="navigation">
        <div className="Navbar__img">
          <Link to="/">
            <img src="/assets/logo3.png" alt="logo"></img>
          </Link>
        </div>
        <div id="menuToggle">
          <input type="checkbox" />

          <span></span>
          <span></span>
          <span></span>

          <ul id="menu" className="Navbar__containerLinks">
            <Link to="/"> Home </Link>
            <Link to="/quienes-somos"> Quiénes Somos </Link>
            <Link to="/que-hacemos"> Qué Hacemos </Link>
            <Link to="/actualidad">Actualidad</Link>
            <Link to="/unete">
           

              <h3 className="Navbar__containerLinks__join" role="button">
                Hazte Socio
              </h3>
            </Link>
           
          </ul>
        </div>
      </nav>
    </div>
  );
}
