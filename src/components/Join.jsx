import React from "react";
import "../styles/Join.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHandHoldingHeart,
  faMinus,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

export default function Join() {
  return (
    <section className="Join">
      <div className="Join__intro">
        <p>
          Gracias
          <span className="bold-1"> por </span>
          <span className="bold-2"> ser parte del cambio y </span> ¡Bienvenido a
          <span className="bold-1"> Abriendo Fronteras!</span>
        </p>
      </div>

      <div className="Join__info">
        <img src="/assets/join-colaborate.jpg" alt="colaborate"></img>

        <ul className="Join__info-reasons">
          <p>Escoge la modalidad que mejor se adapte a tus necesidades:</p>
          <li className="reason-1">
            <FontAwesomeIcon icon={faHandHoldingHeart} />
            <a href="#monthly">Cuota Mensual </a> de 3, 7 ó 10€
          </li>

          <li className="reason-2">
          <FontAwesomeIcon
                className="reason-2__container__icon"
                icon={faHandHoldingHeart}
              />
            <span className="reason-2__container">
      
              Donación puntual, de la cantidad que desees.
             
              <span className="reason-2__container__paragraph">
                Solo tienes que hacer clic en{" "}
                <Link to="/donate">este enlace</Link> y rellenar los datos.{" "}
              </span>
            </span>
          </li>
          <li className="reason-3">
            <span className="reason-3__container">
              <FontAwesomeIcon
                className="reason-3__container__icon"
                icon={faHandHoldingHeart}
              />
              Si eres empresa:
            </span>
            <ul>
              <li>
                <FontAwesomeIcon icon={faMinus} />
                Financia el proyecto de Abriendo Fronteras que más te guste, ya
                sea de forma mensual, trimestral, semestral o anual.
              </li>
              <li>
                <FontAwesomeIcon icon={faMinus} />
                Financia uno o varios de nuestros proyectos haciendo una única
                donación.
              </li>
              <li>
                <FontAwesomeIcon icon={faMinus} />
                Producto solidario: destina a Abriendo Fronteras un porcentaje
                de uno de los productos que vendas o cobra 1, 3 o 5 euros más
                por él y dona esa cantidad a nuestra asociación.
              </li>
              <li>
                <FontAwesomeIcon icon={faMinus} />
                Haz una donación material
              </li>
              <li>
                <FontAwesomeIcon icon={faMinus} />
                Challenge
              </li>
            </ul>
          </li>
          <li>
            <FontAwesomeIcon icon={faHandHoldingHeart} />
            Otras formas de ayudar: Infórmate aquí sobre los voluntariados
            disponibles en Abriendo Fronteras
          </li>
        </ul>
      </div>

      <div className="Join__faqs">
        <div className="Join__faqs__content">
          <h3>PREGUNTAS FRECUENTES</h3>
          <details>
            <summary>¿Cómo puedo hacerme socia/o?</summary>
            <p>
              Es muy sencillo. Haz click <Link to="/donate">aquí</Link> y rellena los datos. Te mandaremos
              un mail de confirmación
            </p>
          </details>

          <details>
            <summary>¿Tengo algún tipo de permanencia?</summary>
            <p>
              No, puedes ser socia/o de Abriendo Fronteras el tiempo que
              consideres.
            </p>
          </details>

          <details>
            <summary>¿Cómo puedo darme de baja?</summary>
            <p>Mándanos un <a href="mailto:info@abriendofronteras.es">mail </a> y automáticamente te daremos de baja.</p>
          </details>

          <details>
            <summary>
              ¿Cómo puedo saber adónde va a parar el dinero que dono?
            </summary>
            <p>
              En nuestro portal de transparencia encontrarás todas las
              justificaciones relacionadas con la gestión de los recursos.
            </p>
          </details>

          <details>
            <summary>¿Puedo cambiar la cantidad de mi cuota</summary>
            <p>
              Por supuesto, tanto si quieres donar una cantidad mayor como una
              cantidad menor, puedes modificar tu preferencia en el siguiente 
              <Link to="/donate">línk.</Link>
            </p>
          </details>

          <details>
            <summary>¿Por qué hacerme socia/o?</summary>
            <p>
              Estás demostrando tu
              compromiso con el cambio social, consiguiendo que las
              personas migrantes se conviertan en hablantes autónomos e
              independientes con nuestras clases de español y alfabetización. 
              Gracias a ti, niñas/os en situación de vulnerabilidad tienen
              regalos de Navidad, porque organizamos entregas de ropa y
              juguetes. Estás fomentando la educación, porque damos clases de
              refuerzo y apoyo escolar a niñas/os con necesidades educativas
              especiales. Dando acceso a terapias psicológicas, ya que
              la <a href="#">Asociación Solidez</a> colabora con Abriendo Fronteras. 
              Estás dando acceso al mundo laboral gracas a las entidades con las que colaboramos, entre
              otras, la <a href="https://empleo.fundacionadecco.org/">Fundación Adecco </a> . Si eres socia/o de Abriendo
              Fronteras, nos recuerdas que nuestro trabajo es importante y nos
              das fuerzas para seguir en nuestra lucha por alcanzar una sociedad
              ecuánime. Gracias por tu apoyo.
            </p>
          </details>

          <details>
            <summary>
              ¿Puedo ser socia/o y hacer, además, una donación puntual?
            </summary>
            <p>Por supuesto, es perfectamente compatible</p>
          </details>

          <details>
            <summary>¿Hay una cantidad mínima?</summary>
            <p>No, la cantidad que aportas es tu decisión.</p>
          </details>

          <details>
            <summary>
              ¿Puedo elegir a qué proyecto o actividad destinar mi donación?
            </summary>
            <p>
              Claro, solo tienes que especificarlo a la hora de hacer la
              donación.
            </p>
          </details>

      
        </div>
      </div>

      <div className="Join__donation">
        <h3>¿Qué cantidad quieres donar?</h3>
        <div id="monthly" className="Join__donation-buttons">
          <Link to="/donate">
            <h3 className="button-1">Abre fronteras por 3€</h3>
          </Link>
          <div className="line"></div>
          <Link to="/donate">
            <h3 className="button-1">Abre fronteras por 7€</h3>
          </Link>
          <div className="line"></div>
          <Link to="/donate">
            <h3 className="button-1">Abre fronteras por 10€</h3>
          </Link>
          <div className="line"></div>
          <Link to="/donate">
            <h3 className="button-1">Otra Cantidad</h3>
          </Link>
        </div>
        <img src="/assets/colaborate.png" alt="colaborate"></img>
      </div>
    </section>
  );
}
