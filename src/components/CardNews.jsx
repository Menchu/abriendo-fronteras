import React from "react";
import "../styles/CardNews.scss";

export default function CardNews(props) {
  return (
    <li className="News__list__notice" key={props.id + props.title}>
      <div className="News__list__notice-body  ">
        <div className="News__list__notice-body-image">
          <img src={props.image} alt={props.title} />
          <p>{props.title}</p>
        </div>

        <div className="News__list__notice-body-text1"></div>
        <div className="News__list__notice-body-text2">
          <p>{props.intro}</p>
          <p>{props.intro2}</p>
          <p>{props.description}</p>
          <p>{props.description2}</p>
          <p>{props.final}</p>
          <span>
            <a href={props.link} alt="FYI links">
              Links de Interés
            </a>
          </span>
        </div>
      </div>
    </li>
  );
}
