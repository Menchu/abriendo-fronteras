import React from "react";
import "../styles/WhatWeDo.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMugHot,
  faGraduationCap,
  faHandsHelping,
  faUsers,
  faHandHoldingHeart,
  faCheck,
} from "@fortawesome/free-solid-svg-icons";

export default function WhatWeDo() {
  return (
    <section className="whatWeDo">
      <div className="whatWeDo__topBlock">
        <div className="whatWeDo__topBlock__text">
          <p>
            “Desarrollamos nuestras acciones poniendo siempre el foco en las
            personas con las que trabajamos, <span className="whatWeDo__topBlock__text-bold">
              verdaderas protagonistas del cambio.”
            </span>
          </p>
        </div>
      </div>

      <div className="whatWeDo__list">
        <div className="whatWeDo__list-item">
          <FontAwesomeIcon className="homeLink" icon={faMugHot} />
          <i className="fas fa-mug-hot"></i>
          <h4>OCIO Y TIEMPO LIBRE</h4>

          <p>
            Realizar actividades fuera de las obligaciones personales ayuda a
            mantener una mente sana y productiva. Abriendo Fronteras ofrece
            actividades de ocio educativo para familias, adultos y menores.
          </p>
        </div>

        <div className="whatWeDo__list-item">
          <FontAwesomeIcon className="homeLink" icon={faGraduationCap} />
          <h4>FORMACIÓN</h4>
          <p>
            La formación es una prioridad para Abriendo Fronteras. El
            intercambio de conocimientos y pautas de intervención entre los/as
            profesionales, es un elemento indispensable para seguir ofreciendo
            un servicio de calidad y adaptar nuestra labora a las necesidades de
            la población.{" "}
          </p>
        </div>

        <div className="whatWeDo__list-item">
          <FontAwesomeIcon className="homeLink" icon={faHandsHelping} />
          <h4>INTEGRACIÓN LABORAL</h4>
          <p>
            El trabajo nos hace libres. Abriendo Fronteras ofrece un servicio de
            orientación laboral individualizado que verte en dos ejes, creando
            un puente entre empresas y personas demandantes de empleo.
          </p>
        </div>

        <div className="whatWeDo__list-item">
          <FontAwesomeIcon className="homeLink" icon={faUsers} />
          <h4>TERAPIA FAMILIAR</h4>
          <p>
            En situaciones de vulnerabilidad, la familia puede ser de los pocos
            entornos en los que sentirnos arropados y protegidos. Abriendo
            Fronteras ofrece atención integral a familias en situación de
            vulnerabilidad social a través de actividades vinculadas a la
            orientación y terapia, así como creación de espacios para trabajar
            la promoción de la salud, desarrollo personal y social.
          </p>
        </div>

        <div className="whatWeDo__list-item">
          <FontAwesomeIcon className="homeLink" icon={faHandHoldingHeart} />
          <h4>SENSIBILIZACIÓN</h4>
          <p>
            Abriendo Fronteras apuesta por una sociedad en la que prima la
            tolerancia y el respeto. Una sociedad en la que la diferencia
            cultural es motivo de celebración y aprendizaje. Es por esto que
            realizamos acciones de Educación para la Sensibilización,
            promoviendo y fomentando el conocimiento mutuo y el intercambio de
            tradiciones, información y experiencias.
          </p>
        </div>
      </div>

      <div className="whatWeDo__adn">
        <p>
          {" "}
          “ En <span className="whatWeDo__adn-bold">
            Abriendo Fronteras{" "}
          </span>{" "}
          trabajamos con colectivos en situación de extrema vulnerabilidad.
          Apostamos por un enfoque integral, interdisciplinario y
          multifactorial, entendiendo la integración como un área de
          intervención que necesita de la actuación conjunta de diferentes
          profesionales.”{" "}
        </p>
      </div>

      <div className="whatWeDo__mission">
        <ul className="whatWeDo__mission-list">
          <li className="whatWeDo__mission-list-element">
            <h4>MISIÓN</h4>
            <p>
              Dar respuesta a las necesidades (sociales, personales y
              materiales) de población en situación de vulnerabilidad social,
              apostando por el desarrollo integral de la persona.
            </p>
          </li>
          <li className="whatWeDo__mission-list-element">
            <h4>VISIÓN</h4>
            <p>
              Llegar a ser una herramienta que posibilite una sociedad
              diferente, abierta, libre, intercultural, cohesionada y
              equitativa. Queremos favorecer la creación de un entorno que
              ofrezca apoyo, seguridad y que fomente el crecimiento personal y
              social de la población.
            </p>
          </li>
          <li className="whatWeDo__mission-list-element">
            <h4>OBJETIVO</h4>
            <p>
              Impulsar proyectos sociales que atiendan las necesidades en
              situación de vulnerabilidad.
            </p>
          </li>
          <li className="whatWeDo__mission-list-element">
            <h4>NUESTROS VALORES</h4>
            <p>
              <FontAwesomeIcon className="homeLink" icon={faCheck} /> Autonomía
            </p>
            <p>
              <FontAwesomeIcon className="homeLink" icon={faCheck} /> Diversidad
            </p>
            <p>
              <FontAwesomeIcon className="homeLink" icon={faCheck} /> Igualdad y
              equidad.
            </p>
            <p>
              <FontAwesomeIcon className="homeLink" icon={faCheck} />{" "}
              Interculturalidad
            </p>
            <p>
              <FontAwesomeIcon className="homeLink" icon={faCheck} />{" "}
              Transparencia
            </p>
            <p>
              <FontAwesomeIcon className="homeLink" icon={faCheck} />{" "}
              Confidencialidad
            </p>
            <p>
              <FontAwesomeIcon className="homeLink" icon={faCheck} /> Compromiso
              con el medio ambiente.
            </p>
          </li>
        </ul>
      </div>
    </section>
  );
}
