import "../styles/Home.scss";
import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faGlobeAmericas, faMoneyBillAlt, faHiking} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import newsJson from "../news.json";

export default function Home() {
  const [news] = useState(newsJson);
  return (
    <section className="Home">
      <div className="Home__intro">
        <p>
          En <span className="bold-1">Abriendo Fronteras</span> trabajamos para
          que el futuro de las personas sea{" "}
          <span className="bold-2"> digno, justo y esperanzador.</span>{" "}
        </p>
      </div>

      <div className="Home__info">
        <ul className="Home__info__block">
          <li className="Home__info__block-element">
            <FontAwesomeIcon className="homeLink" icon={faGlobeAmericas} />
            <h4>
              9,7 millones de personas están en riesgo de pobreza en España
            </h4>
          </li>
          <li className="Home__info__block-element">
            <FontAwesomeIcon className="homeLink" icon={faMoneyBillAlt} />
            <h4>
              En las economías del sur de Europa, el crecimiento del PIB habría
              sido entre 20 y 30 puntos más bajo si no se contara con la
              población inmigrante.
            </h4>
          </li>
          <li className="Home__info__block-element">
            <FontAwesomeIcon className="homeLink" icon={faHiking} />
            <h4>
              La tasa de reconocimiento del estatuto del refugiado fue del 2,64
              % en 2019
            </h4>
          </li>
        </ul>
      </div>
      <section className="Home__containerBotton">
        <div className="Home__containerBotton__teamLink">
          <h5 className="Home__containerBotton__teamLink-paragraph ">
            Queremos que formes parte de una realidad distinta y conozocas todo
            nuestro trabajo
          </h5>

          <Link to="/quienes-somos">Conoce a nuestro Equipo</Link>
        </div>

        <div className="Home__containerBotton__newsFiltered">
          <div className="Home__containerBotton__newsFiltered__label">
            <p>
              Visita nuestra sección de Noticias y entérate de todas nuestras
              acciones y ¡Únete!
            </p>
          </div>
          <ul className="Home__containerBotton__newsFiltered__list">
            {news.filter((notice) => notice.type === "new")
              .map((filteredNews) => (
                <li className="Home__containerBotton__newsFiltered__list-notice" key={filteredNews.id}>
                  <div>
                    <h4>{filteredNews.title}</h4>
                    <p>{filteredNews.intro}</p>
                    <Link to="/actualidad"> Leer más </Link>
                   
                  </div>
                </li>
              ))}
          </ul>
        </div>
      </section>
    </section>
  );
}
