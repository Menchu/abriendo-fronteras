import React from "react";
import "../../styles/WhoWeAre.scss";

export default function WhoWeAre(props) {
  return (
    <section className="WhoWeAre">
      <h2>Conoce a Nuestro Equipo</h2>
      <ul className="WhoWeAre__list">
        {props.team.map((member) => {
          return (
            <li className="WhoWeAre__list__member" key={member.id}>
              <img
                className="WhoWeAre__list__member-image"
                src={member.image}
                alt={props.name}
              />

              <div className="WhoWeAre__list__member-info">
                <p className="WhoWeAre__list__member-info-name">
                  {" "}
                  {member.name}{" "}
                </p>
                <p className="WhoWeAre__list__member-info-position">
                  {member.position}
                </p>
                <p className="WhoWeAre__list__member-info-description">
                  {member.description}
                </p>
              </div>
            </li>
          );
        })}
      </ul>
    </section>
  );
}
