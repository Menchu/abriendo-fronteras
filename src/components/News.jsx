import React, { useState } from "react";
import "../styles/News.scss";
import newsJson from "../news.json";
import CardNews from "./CardNews";

export default function News() {
  const [news] = useState(newsJson);

  return (
    <section className="News">
      <div className="News__intro">
        <p>
          Bienvenido a nuestra sección de Noticias, entérate de todas nuestras
          acciones y ¡súmate!
        </p>
      </div>
      <ul className="News__list" key={news.id}>
        {news.map(
          ({
            id,
            title,
            intro,
            intro2,
            description,
            description2,
            link,
            final,
            image,
            type,
          }) => (
            <CardNews
              id={id}
              title={title}
              intro={intro}
              intro2={intro2}
              description={description}
              description2={description2}
              link={link}
              final={final}
              image={image}
              type={type}
            />
          )
        )}
      </ul>
    </section>
  );
}
